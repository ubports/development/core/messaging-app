# Catalan translation for messaging-app
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the messaging-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: messaging-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-03 08:08+0000\n"
"PO-Revision-Date: 2022-05-23 11:46+0000\n"
"Last-Translator: Joan CiberSheep <cibersheep@gmail.com>\n"
"Language-Team: Catalan <https://translate.ubports.com/projects/ubports/"
"messaging-app/ca/>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2016-12-13 05:38+0000\n"

#: lomiri-messaging-app.desktop.in:4
msgid "Messaging"
msgstr "Missatgeria"

#: lomiri-messaging-app.desktop.in:5
msgid "Messaging App"
msgstr "Aplicació de missatgeria"

#: lomiri-messaging-app.desktop.in:6 src/messagingapplication.cpp:208
msgid "Messaging application"
msgstr "Aplicació de missatgeria"

#: lomiri-messaging-app.desktop.in:7
msgid "Messages;SMS;MMS;Text Messages;Text"
msgstr "Missatges;missatgeria;SMS;MMS;missatge de text;text"

#: src/qml/AccountSectionDelegate.qml:89
#, qt-format
msgid "%1 was invited to this group"
msgstr "S'ha convidat %1 al grup"

#: src/qml/AccountSectionDelegate.qml:91
#, qt-format
msgid "You invited %1 to this group"
msgstr "Heu convidat %1 al grup"

#: src/qml/AccountSectionDelegate.qml:93
#, qt-format
msgid "%1 invited %2 to this group"
msgstr "%1 ha convidat %2 al grup"

#: src/qml/AccountSectionDelegate.qml:96
#, qt-format
msgid "You switched to %1 @ %2"
msgstr "Heu commutat a %1 (%2)"

#: src/qml/AccountSectionDelegate.qml:100
msgid "You left this group"
msgstr "Heu marxat d'aquest grup"

#: src/qml/AccountSectionDelegate.qml:103
#, qt-format
msgid "Renamed group to: %1"
msgstr "S'ha reanomenat el grup com: %1"

#: src/qml/AccountSectionDelegate.qml:105
#, qt-format
msgid "You renamed group to: %1"
msgstr "Heu reanomenat el grup com: %1"

#: src/qml/AccountSectionDelegate.qml:107
#, qt-format
msgid "%1 renamed group to: %2"
msgstr "%1 ha reanomenat el grup com: %2"

#: src/qml/AccountSectionDelegate.qml:111
#, qt-format
msgid "%1 removed %2 from this group"
msgstr "%1 ha eliminat %2 del grup"

#: src/qml/AccountSectionDelegate.qml:113
#, qt-format
msgid "%1 left this group"
msgstr "%1 ha abandonat el grup"

#: src/qml/AccountSectionDelegate.qml:116
msgid "You joined this group"
msgstr "Us heu afegit al grup"

#: src/qml/AccountSectionDelegate.qml:119
#, qt-format
msgid "%1 added %2 to this group"
msgstr "%1 ha afegit %2 al grup"

#: src/qml/AccountSectionDelegate.qml:121
#, qt-format
msgid "%1 joined this group"
msgstr "%1 s'ha afegit al grup"

#: src/qml/AccountSectionDelegate.qml:125
#, qt-format
msgid "%1 set %2 as Admin"
msgstr "%1 ha fet %2 administrador/a"

#: src/qml/AccountSectionDelegate.qml:127
#, qt-format
msgid "%1 is Admin"
msgstr "%1 és l'administrador/a"

#: src/qml/AccountSectionDelegate.qml:130
msgid "You are Admin"
msgstr "Sou administrador/a"

#: src/qml/AccountSectionDelegate.qml:133
#, qt-format
msgid "%1 set %2 as not Admin"
msgstr "%1 ha fet que %2 no sigui administrador/a"

#: src/qml/AccountSectionDelegate.qml:135
#, qt-format
msgid "%1 is not Admin"
msgstr "%1 no és l'administrador/a"

#: src/qml/AccountSectionDelegate.qml:138
msgid "You are not Admin"
msgstr "No sou administrador/a"

#: src/qml/AccountSectionDelegate.qml:140
msgid "You were removed from this group"
msgstr "Heu estat expulsat d'aquest grup"

#: src/qml/AccountSectionDelegate.qml:142 src/qml/GroupChatInfoPage.qml:240
msgid "Group has been dissolved"
msgstr "El grup s'ha dissolt"

#: src/qml/AttachmentDelegates/ContactDelegate.qml:48
#: src/qml/ThumbnailContact.qml:44
msgid "Unknown contact"
msgstr "Contacte desconegut"

#: src/qml/AttachmentDelegates/DefaultDelegate.qml:28
msgid "Audio attachment not supported"
msgstr "L'adjunció d'àudio no és compatible"

#: src/qml/AttachmentDelegates/DefaultDelegate.qml:31
msgid "Video attachment not supported"
msgstr "L'adjunció de vídeo no és compatible"

#: src/qml/AttachmentDelegates/DefaultDelegate.qml:33
msgid "File type not supported"
msgstr "El tipus de fitxer no és compatible"

#: src/qml/AttachmentDelegates/PreviewerImage.qml:36
msgid "Image Preview"
msgstr "Previsualització de la imatge"

#: src/qml/AttachmentDelegates/PreviewerMultipleContacts.qml:91
#: src/qml/AttachmentDelegates/Previewer.qml:86
#: src/qml/MessagingContactEditorPage.qml:49
msgid "Save"
msgstr "Desa"

#: src/qml/AttachmentDelegates/PreviewerMultipleContacts.qml:99
#: src/qml/AttachmentDelegates/Previewer.qml:93
#: src/qml/MessagingContactViewPage.qml:85
msgid "Share"
msgstr "Comparteix"

#: src/qml/AttachmentDelegates/Previewer.qml:62
#: src/qml/Dialogs/EmptyGroupWarningDialog.qml:40
#: src/qml/Dialogs/MMSBroadcastDialog.qml:45
#: src/qml/Dialogs/MMSEnableDialog.qml:46
#: src/qml/Dialogs/RemoveThreadDialog.qml:43
#: src/qml/Dialogs/SetDefaultSIMCardDialog.qml:39
#: src/qml/GroupChatInfoPage.qml:396 src/qml/MainPage.qml:157
#: src/qml/MessagingContactEditorPage.qml:36 src/qml/NewRecipientPage.qml:168
#: src/qml/OnlineAccountsHelper.qml:73
msgid "Cancel"
msgstr "Cancel·la"

#: src/qml/AttachmentDelegates/PreviewerVideo.qml:32
msgid "Video Preview"
msgstr "Previsualització de vídeo"

#: src/qml/AttachmentPanel.qml:104
msgid "Image"
msgstr "Imatge"

#: src/qml/AttachmentPanel.qml:117
msgid "Video"
msgstr "Vídeo"

#: src/qml/AttachmentPanel.qml:155
msgid "Contact"
msgstr "Contacte"

#: src/qml/AudioRecordingBar.qml:154
msgid "<<< Swipe to cancel"
msgstr "<<< Llisqueu per cancel·lar"

#: src/qml/ComposeBar.qml:203 src/qml/Dialogs/EmptyGroupWarningDialog.qml:48
#: src/qml/GroupChatInfoPage.qml:432 src/qml/GroupChatInfoPage.qml:436
#: src/qml/Stickers/StickersPicker.qml:118
msgid "Remove"
msgstr "Elimina"

#: src/qml/ComposeBar.qml:227
msgid "You have to press and hold the record icon"
msgstr "Polseu i aguanteu la icona de gravació"

#: src/qml/ComposeBar.qml:569
msgid "Write a broadcast message..."
msgstr "Escriviu un missatge de difusió…"

#: src/qml/ComposeBar.qml:579
msgid "Write a message..."
msgstr "Escriviu un missatge..."

#: src/qml/ComposeBar.qml:624 src/qml/MessageAlertBubble.qml:191
#: src/qml/MessageDelegate.qml:161
msgid "MMS"
msgstr "MMS"

#: src/qml/ContactSearchWidget.qml:44
msgid "Members:"
msgstr "Membres:"

#: src/qml/ContactSearchWidget.qml:67
msgid "Number or contact name"
msgstr "Número o nom del contacte"

#: src/qml/dateUtils.js:44
msgid "Today"
msgstr "Avui"

#: src/qml/dateUtils.js:49
msgid "Yesterday"
msgstr "Ahir"

#: src/qml/Dialogs/EmptyGroupWarningDialog.qml:30
#, qt-format
msgid ""
"Removing last member will cause '%1' to be dissolved. Would you like to "
"continue?"
msgstr "Eliminar el darrer membre farà que «%1» es dissolgui. Voleu continuar?"

#: src/qml/Dialogs/FileSizeWarningDialog.qml:26
msgid "File size warning"
msgstr "Advertiment de mida de fitxer"

#: src/qml/Dialogs/FileSizeWarningDialog.qml:37
msgid ""
"You are trying to send big files (over 300Kb). Some operators might not be "
"able to send it."
msgstr ""
"Esteu intentant enviar fitxers grans (més de 300Kb). Pot ser que alguns "
"operadors no els puguin enviar."

#: src/qml/Dialogs/FileSizeWarningDialog.qml:45
#: src/qml/Dialogs/NoMicrophonePermission.qml:49
#: src/qml/Dialogs/SimLockedDialog.qml:49
msgid "Ok"
msgstr "D'acord"

#: src/qml/Dialogs/FileSizeWarningDialog.qml:59
msgid "Don't show again"
msgstr "No mostris un altre cop"

#: src/qml/Dialogs/InformationDialog.qml:30
#: src/qml/Dialogs/NoDefaultSIMCardDialog.qml:47
#: src/qml/Dialogs/NoNetworkDialog.qml:32 src/qml/MessageInfoDialog.qml:167
msgid "Close"
msgstr "Tanca"

#: src/qml/Dialogs/MMSBroadcastDialog.qml:30
msgid "Multiple MMS Messages"
msgstr "Múltiples missatges MMS"

#: src/qml/Dialogs/MMSBroadcastDialog.qml:31
msgid ""
"The content you are sending requires one MMS message per recipient, so you "
"might get charged for multiple messages.\n"
"Do you want to continue?"
msgstr ""
"El contingut que estau enviat requereix un missatge MMS per destinatari/a, "
"se us podria cobrar per múltiples missatges.\n"
"Voleu continuar?"

#: src/qml/Dialogs/MMSBroadcastDialog.qml:35
msgid "Send"
msgstr "Envia"

#: src/qml/Dialogs/MMSEnableDialog.qml:30
msgid "MMS support required"
msgstr "Es requereix suport dels MMS"

#: src/qml/Dialogs/MMSEnableDialog.qml:31
msgid ""
"MMS support is required to send this message.\n"
"Do you want to enable it?"
msgstr ""
"Es requereix suport pels MMS per enviar aquest missatge.\n"
"Voleu habilitar-ho?"

#: src/qml/Dialogs/MMSEnableDialog.qml:35
msgid "Enable"
msgstr "Activa"

#: src/qml/Dialogs/NoDefaultSIMCardDialog.qml:27
msgid "Welcome to your Messaging app!"
msgstr "Us donem la benvinguda a l'aplicació de missatgeria!"

#: src/qml/Dialogs/NoDefaultSIMCardDialog.qml:38
msgid ""
"If you wish to edit your SIM and other mobile preferences, please visit <a "
"href=\"system_settings\">System Settings</a>."
msgstr ""
"Si voleu editar el vostre SIM i altres preferències mòbils, si us plau "
"visiteu <a href=\"system_settings\">Paràmetres del sistema</a>."

#: src/qml/Dialogs/NoMicrophonePermission.qml:26
msgid "No permission to access microphone"
msgstr "No teniu permisos per accedir el micròfon"

#: src/qml/Dialogs/NoMicrophonePermission.qml:37
msgid ""
"Please grant access on <a href=\"system_settings\">System Settings &gt; "
"Security &amp; Privacy</a>."
msgstr ""
"Si us plau permeteu l'accés a <a href=\"system_settings\">Paràmetres del "
"sistema &gt; Seguretat i privadesa</a>."

#: src/qml/Dialogs/NoNetworkDialog.qml:28
msgid "No network"
msgstr "Sense xarxa"

#: src/qml/Dialogs/NoNetworkDialog.qml:29
#, qt-format
msgid "There is currently no network on %1"
msgstr "No hi ha cap xarxa a %1"

#: src/qml/Dialogs/NoNetworkDialog.qml:29
msgid "There is currently no network."
msgstr "No hi ha cap xarxa actualment."

#: src/qml/Dialogs/RemoveThreadDialog.qml:28
#, fuzzy
#| msgid "Deleted"
msgid "Delete thread"
msgid_plural "Delete threads"
msgstr[0] "Suprimit"
msgstr[1] "Suprimit"

#: src/qml/Dialogs/RemoveThreadDialog.qml:30
#, fuzzy, qt-format
#| msgid "Deleted"
msgid "Delete this thread?"
msgid_plural "Delete %1 threads?"
msgstr[0] "Suprimit"
msgstr[1] "Suprimit"

#: src/qml/Dialogs/RemoveThreadDialog.qml:47 src/qml/MessageAlertBubble.qml:170
#: src/qml/MessageDelegate.qml:127 src/qml/NewGroupPage.qml:309
#: src/qml/RegularMessageDelegate_irc.qml:151 src/qml/ThreadDelegate.qml:179
msgid "Delete"
msgstr "Elimina"

#. TRANSLATORS: %1 refers to the SIM card name or account name
#: src/qml/Dialogs/SetDefaultSIMCardDialog.qml:29
#, qt-format
msgid "Change all Messaging associations to %1?"
msgstr "Voleu canviar totes les associacions de missatgeria a %1?"

#: src/qml/Dialogs/SetDefaultSIMCardDialog.qml:47
msgid "Change"
msgstr "Canvia"

#: src/qml/Dialogs/SetDefaultSIMCardDialog.qml:62
msgid "Don't ask again"
msgstr "No m'ho tornis a demanar"

#: src/qml/Dialogs/SimLockedDialog.qml:26
msgid "SIM Card is locked"
msgstr "La targeta SIM està blocada"

#: src/qml/Dialogs/SimLockedDialog.qml:37
msgid ""
"Please unlock your SIM card to call or send a message. You can unlock your "
"SIM card from the Network Indicator at the top of the screen or by visiting "
"<a href=\"system_settings\">System Settings &gt; Security &amp; Privacy</a>."
msgstr ""
"Heu de desblocar la targeta SIM abans de poder trucar o enviar missatges de "
"text. Podeu fer-ho des de l'indicador de xarxes a la part superior de la "
"pantalla, o bé si aneu a <a href=\"system_settings\">Configuració del "
"sistema &gt; Seguretat i privadesa</a>."

#: src/qml/EmptyState.qml:50
msgid "Compose a new message by swiping up from the bottom of the screen."
msgstr ""
"Redacteu un missatge nou fent lliscar el dit des de la vora inferior de la "
"pantalla."

#: src/qml/GroupChatInfoPage.qml:112
msgid "Leave channel"
msgstr "Deixa el canal"

#: src/qml/GroupChatInfoPage.qml:114
msgid "Leave group"
msgstr "Deixa el grup"

#: src/qml/GroupChatInfoPage.qml:120
msgid "Channel Info"
msgstr "Informació del canal"

#: src/qml/GroupChatInfoPage.qml:122
msgid "Group Info"
msgstr "Informació del grup"

#: src/qml/GroupChatInfoPage.qml:128
msgid "Successfully left channel"
msgstr "Heu abandonat el canal amb èxit"

#: src/qml/GroupChatInfoPage.qml:130
msgid "Successfully left group"
msgstr "Heu abandonat el grup amb èxit"

#: src/qml/GroupChatInfoPage.qml:136
msgid "Failed to leave channel"
msgstr "No s'ha pogut abandonar el grup"

#: src/qml/GroupChatInfoPage.qml:138
msgid "Failed to leave group"
msgstr "No s'ha pogut abandonar el grup"

#: src/qml/GroupChatInfoPage.qml:149
msgid "Me"
msgstr "Jo"

#: src/qml/GroupChatInfoPage.qml:182
msgid "End group"
msgstr "Finalitza el grup"

#: src/qml/GroupChatInfoPage.qml:213 src/qml/GroupChatInfoPage.qml:219
#: src/qml/GroupChatInfoPage.qml:225 src/qml/NewGroupPage.qml:59
msgid "This recipient was already selected"
msgstr "Ja heu seleccionat aquest destinatari/a"

#: src/qml/GroupChatInfoPage.qml:238
msgid "Failed to delete group"
msgstr "No s'ha pogut eliminar el grup"

#: src/qml/GroupChatInfoPage.qml:248
msgid "Failed to modify group title"
msgstr "No s'ha pogut modificar el títol del grup"

#: src/qml/GroupChatInfoPage.qml:374
#, qt-format
msgid "Participants: %1"
msgstr "Participants: %1"

#: src/qml/GroupChatInfoPage.qml:374
msgid "Add participant:"
msgstr "Afegeix participant:"

#: src/qml/GroupChatInfoPage.qml:396
msgid "Add..."
msgstr "Afegeix…"

#: src/qml/ListItemDemo.qml:58 src/qml/PinchToZoomDemo.qml:76
#, fuzzy
#| msgid "Welcome to your Ubuntu messaging app."
msgid "Welcome to your Ubuntu Touch messaging app."
msgstr "Us donem la benvinguda a l'apli de missatgeria."

#: src/qml/ListItemDemo.qml:139 src/qml/PinchToZoomDemo.qml:269
msgid "Got it"
msgstr "D'acord"

#: src/qml/ListItemDemo.qml:165
msgid "Swipe to reveal actions"
msgstr "Llisqueu el dit per mostrar accions"

#: src/qml/ListItemDemo.qml:215
msgid "Swipe to delete"
msgstr "Llisqueu el dit per suprimir"

#: src/qml/MainPage.qml:73 src/qml/Messages.qml:693
#: src/qml/NewRecipientPage.qml:130
msgid "Search..."
msgstr "Cerca..."

#: src/qml/MainPage.qml:88
msgid "Messages"
msgstr "Missatges"

#: src/qml/MainPage.qml:111 src/qml/Messages.qml:897 src/qml/Messages.qml:1023
#: src/qml/NewRecipientPage.qml:141
msgid "Search"
msgstr "Cerca"

#: src/qml/MainPage.qml:121 src/qml/SettingsPage.qml:26
msgid "Settings"
msgstr "Configuració"

#: src/qml/MainPage.qml:130 src/qml/MainPage.qml:276
msgid "New message"
msgstr "Missatge nou"

#: src/qml/MainPage.qml:210
msgid "Select"
msgstr "Selecciona"

#: src/qml/MainPage.qml:231
#, qt-format
msgid "%1 - Connecting..."
msgstr "%1 - Connectant…"

#: src/qml/MessageAlertBubble.qml:34
msgid ""
"Could not fetch the MMS message. Maybe the MMS settings are incorrect or "
"cellular data is off? Ask to have the message sent again if everything is OK."
msgstr ""

#: src/qml/MessageAlertBubble.qml:35
msgid ""
"Could not fetch the MMS message. Maybe the MMS settings are incorrect or "
"cellular data is off?"
msgstr ""

#: src/qml/MessageAlertBubble.qml:36
#, fuzzy, qt-format
#| msgid "New MMS message to download: size is %1KB, expires on %2"
msgid "New MMS message (of %1 kB) to be downloaded before %2"
msgstr "Nou missatge MMS per descarregar: mida %1KB, caduca en %2"

#: src/qml/MessageAlertBubble.qml:145
msgid "Download"
msgstr "Descarrega"

#: src/qml/MessageAlertBubble.qml:179 src/qml/MessageDelegate.qml:159
#: src/qml/ParticipantInfoPage.qml:35
#: src/qml/RegularMessageDelegate_irc.qml:186
msgid "Info"
msgstr "Informació"

#: src/qml/MessageAlertBubble.qml:200 src/qml/MessageDelegate.qml:170
msgid "Myself"
msgstr "Jo"

#: src/qml/MessageDelegate.qml:94 src/qml/RegularMessageDelegate_irc.qml:66
msgid "Text message copied to clipboard"
msgstr "S'ha copiat el missatge de text al porta-retalls"

#: src/qml/MessageDelegate.qml:136 src/qml/RegularMessageDelegate_irc.qml:163
msgid "Retry"
msgstr "Torna-ho a provar"

#: src/qml/MessageDelegate.qml:144 src/qml/RegularMessageDelegate_irc.qml:171
msgid "Copy"
msgstr "Copia"

#: src/qml/MessageDelegate.qml:152 src/qml/RegularMessageDelegate_irc.qml:179
msgid "Forward"
msgstr "Reenvia"

#: src/qml/MessageDelegate.qml:161
msgid "SMS"
msgstr "SMS"

#: src/qml/MessageInfoDialog.qml:54
msgid "Delivered"
msgstr "Enviat"

#: src/qml/MessageInfoDialog.qml:56
msgid "Temporarily Failed"
msgstr "S'ha produït una fallada temporal"

#: src/qml/MessageInfoDialog.qml:58 src/qml/Messages.qml:320
msgid "Failed"
msgstr "Ha fallat"

#: src/qml/MessageInfoDialog.qml:60
msgid "Accepted"
msgstr "Acceptat"

#: src/qml/MessageInfoDialog.qml:62 src/qml/MessageInfoDialog.qml:156
msgid "Read"
msgstr "Llegit"

#: src/qml/MessageInfoDialog.qml:64
msgid "Deleted"
msgstr "Suprimit"

#: src/qml/MessageInfoDialog.qml:66 src/qml/ParticipantDelegate.qml:45
msgid "Pending"
msgstr "Pendent"

#: src/qml/MessageInfoDialog.qml:70 src/qml/MessageInfoDialog.qml:149
msgid "Received"
msgstr "Rebut"

#: src/qml/MessageInfoDialog.qml:72 src/qml/MessageInfoDialog.qml:75
#: src/qml/MessageInfoDialog.qml:91
msgid "Unknown"
msgstr "Desconegut"

#: src/qml/MessageInfoDialog.qml:87
msgid "Group"
msgstr "Grup"

#: src/qml/MessageInfoDialog.qml:95
msgid "Message info"
msgstr "Informació del missatge"

#: src/qml/MessageInfoDialog.qml:98
msgid "Type"
msgstr "Tipus"

#: src/qml/MessageInfoDialog.qml:102
msgid "From"
msgstr "De"

#: src/qml/MessageInfoDialog.qml:108
msgid "To"
msgstr "A"

#: src/qml/MessageInfoDialog.qml:143
msgid "Sent"
msgstr "Enviat"

#: src/qml/MessageInfoDialog.qml:162
msgid "Status"
msgstr "Estat"

#: src/qml/Messages.qml:99 src/qml/ThreadDelegate.qml:257
msgid "Cell Broadcast"
msgstr ""

#: src/qml/Messages.qml:311 src/qml/SendMessageValidator.qml:211
msgid "You have to disable flight mode"
msgstr "Heu de desactivar el mode avió"

#: src/qml/Messages.qml:312 src/qml/SendMessageValidator.qml:212
msgid "It is not possible to send messages in flight mode"
msgstr "No es poden enviar missatges en mode avió"

#: src/qml/Messages.qml:314 src/qml/SendMessageValidator.qml:214
msgid "No SIM card selected"
msgstr "No s'ha seleccionat cap targeta SIM"

#: src/qml/Messages.qml:315 src/qml/SendMessageValidator.qml:215
msgid "You need to select a SIM card"
msgstr "Cal que seleccioneu una targeta SIM"

#: src/qml/Messages.qml:317 src/qml/SendMessageValidator.qml:217
msgid "No SIM card"
msgstr "No hi ha cap targeta SIM"

#: src/qml/Messages.qml:318 src/qml/SendMessageValidator.qml:218
msgid "Please insert a SIM card and try again."
msgstr "Si us plau inseriu una targeta SIM i torneu a provar."

#: src/qml/Messages.qml:321
msgid "It is not possible to send messages at the moment"
msgstr "No es poden enviar missatges en aquest moment"

#: src/qml/Messages.qml:331
msgid "Not available"
msgstr "No disponible"

#: src/qml/Messages.qml:332
msgid "The selected account is not available at the moment"
msgstr "El compte seleccionat no està disponible en aquest moment"

#: src/qml/Messages.qml:515
msgid ""
"The SIM card does not provide the owner's phone number. Because of that "
"sending MMS group messages is not possible."
msgstr ""
"La targeta SIM no proporciona el número de telèfon del propietari/a. Per aio "
"no és possible l’enviament de missatges de grup MMS."

#: src/qml/Messages.qml:672 src/qml/Messages.qml:748
#: src/qml/NewRecipientPage.qml:99 src/qml/NewRecipientPage.qml:367
#: src/qml/SettingsPage.qml:133 src/qml/SettingsPage.qml:287
msgid "Back"
msgstr "Enrere"

#: src/qml/Messages.qml:871
#, qt-format
msgid "Group (%1)"
msgstr "Grup (%1)"

#: src/qml/Messages.qml:908 src/qml/Messages.qml:1034
msgid "Call"
msgstr "Telefona"

#: src/qml/Messages.qml:920
msgid "Add"
msgstr "Afegeix"

#: src/qml/Messages.qml:1157
msgid "Create MMS Group..."
msgstr "Crea grup MMS…"

#: src/qml/Messages.qml:1181
msgid "Join IRC Channel..."
msgstr "Afegir-se al canal d'IRC…"

#: src/qml/Messages.qml:1187
#, qt-format
msgid "Create %1 Group..."
msgstr "Crea el grup %1…"

#: src/qml/Messages.qml:1335
#, qt-format
msgid "%1 is typing..."
msgstr "%1 està escrivint…"

#: src/qml/Messages.qml:1337
msgid "Typing..."
msgstr "Escrivint…"

#: src/qml/Messages.qml:1346
msgid "Online"
msgstr "En línia"

#: src/qml/Messages.qml:1348
msgid "Offline"
msgstr "Fora de línia"

#: src/qml/Messages.qml:1350
msgid "Away"
msgstr "Absent"

#: src/qml/Messages.qml:1352
msgid "Busy"
msgstr "Ocupat"

#: src/qml/Messages.qml:1737
msgid ""
"You can't send messages to this group because the group is no longer active"
msgstr "No podeu enviar missatges a aquest grup perquè ja no està actiu"

#: src/qml/MessageStatusIcon.qml:75
msgid "Failed!"
msgstr "Ha fallat!"

#: src/qml/MessagingBottomEdge.qml:26
msgid "+"
msgstr "+"

#: src/qml/MessagingContactViewPage.qml:96
msgid "Edit"
msgstr "Edita"

#: src/qml/MultiRecipientInput.qml:33
msgid "To:"
msgstr "Per a:"

#: src/qml/NewGroupPage.qml:81
msgid "Creating Group..."
msgstr "S'està creant el grup…"

#: src/qml/NewGroupPage.qml:84
msgid "New MMS Group"
msgstr "Nou grup MMS"

#: src/qml/NewGroupPage.qml:88
msgid "Join IRC channel:"
msgstr "Afegir-se al canal d'IRC:"

#: src/qml/NewGroupPage.qml:94
#, qt-format
msgid "New %1 Group"
msgstr "Grup nou %1"

#: src/qml/NewGroupPage.qml:170
msgid "Failed to create group"
msgstr "No s'ha pogut crear el grup"

#: src/qml/NewGroupPage.qml:232
msgid "Channel name:"
msgstr "Nom del canal:"

#: src/qml/NewGroupPage.qml:234
msgid "Group name:"
msgstr "Nom del grup:"

#: src/qml/NewGroupPage.qml:250
msgid "#channelName"
msgstr "#nomDelCanal"

#: src/qml/NewGroupPage.qml:252
msgid "Type a name..."
msgstr "Escriviu un nom…"

#: src/qml/NewRecipientPage.qml:90
msgid "Add recipient"
msgstr "Afegeix el destinatari"

#: src/qml/NewRecipientPage.qml:114
msgid "All"
msgstr "Tots"

#: src/qml/NewRecipientPage.qml:114
msgid "Favorites"
msgstr "Preferits"

#: src/qml/NewRecipientPage.qml:222
#, fuzzy
#| msgid "Contact"
msgid "Contact Details"
msgstr "Contacte"

#: src/qml/NewRecipientPage.qml:286
msgid "Please select a phone number"
msgstr ""

#: src/qml/NewRecipientPage.qml:299
#, fuzzy
#| msgid "Members:"
msgid "Numbers"
msgstr "Membres:"

#: src/qml/OnlineAccountsHelper.qml:39
msgid "Pick an account to create."
msgstr "Trieu un compte per crear."

#: src/qml/ParticipantDelegate.qml:42
msgid "Admin"
msgstr "Administrador/a"

#: src/qml/ParticipantInfoPage.qml:135
msgid "See in contacts"
msgstr "Veure als contactes"

#: src/qml/ParticipantInfoPage.qml:135
msgid "Add to contacts"
msgstr "Afegeix als contactes"

#: src/qml/ParticipantInfoPage.qml:153
msgid "Set as admin"
msgstr "Fes administrador/a"

#: src/qml/ParticipantInfoPage.qml:166
msgid "Send private message"
msgstr "Envia un missatge privat"

#: src/qml/ParticipantInfoPage.qml:181
msgid "Remove from group"
msgstr "Elimina del grup"

#: src/qml/PinchToZoomDemo.qml:66
msgid "Hello there!"
msgstr ""

#: src/qml/PinchToZoomDemo.qml:128
msgid "Pinch to decrease font size"
msgstr ""

#: src/qml/PinchToZoomDemo.qml:179
msgid "Spread to increase font size"
msgstr ""

#: src/qml/RegularMessageDelegate_irc.qml:188
msgid "IRC"
msgstr "IRC"

#: src/qml/SendMessageValidator.qml:220
msgid "It is not possible to send the message"
msgstr "No es pot enviar el missatge"

#: src/qml/SendMessageValidator.qml:221
msgid "Failed to send the message"
msgstr "No s'ha pogut enviar el missatge"

#: src/qml/SettingsPage.qml:46
msgid "Sort by timestamp"
msgstr "Ordena per data i hora"

#: src/qml/SettingsPage.qml:47
msgid "Sort by title"
msgstr "Ordena per títol"

#: src/qml/SettingsPage.qml:51
msgid "System Theme"
msgstr "Tema del sistema"

#: src/qml/SettingsPage.qml:52
msgid "Light"
msgstr "Clar"

#: src/qml/SettingsPage.qml:53
msgid "Dark"
msgstr "Fosc"

#: src/qml/SettingsPage.qml:59
msgid "Enable MMS messages"
msgstr "Habilita els missatges MMS"

#: src/qml/SettingsPage.qml:66
msgid "Enable stickers"
msgstr "Habilita els adhesius"

#: src/qml/SettingsPage.qml:73
msgid "Simplified conversation view"
msgstr "Vista de conversa simplificada"

#: src/qml/SettingsPage.qml:80
msgid "Sort threads"
msgstr "Ordena els fils"

#: src/qml/SettingsPage.qml:88
msgid "AutoPlay animated image"
msgstr "Reprodueix les animacions automàticament"

#: src/qml/SettingsPage.qml:95
msgid "Auto display keyboard"
msgstr "Mostra el teclat automàticament"

#: src/qml/SettingsPage.qml:102
msgid "Theme"
msgstr "Tema"

#: src/qml/Stickers/StickersPicker.qml:132
msgid "Stickers"
msgstr "Adhesius"

#: src/qml/Stickers/StickersPicker.qml:133
msgid "Please confirm that you want to delete all stickers in this pack"
msgstr "Confirmeu que voleu eliminar tots els adhesius d'aquest paquet"

#: src/qml/Stickers/StickersPicker.qml:133
msgid "Please confirm that you want to delete all stickers in the history"
msgstr "Confirmeu que voleu eliminar tots els adhesius a l'historial"

#: src/qml/Stickers/StickersPicker.qml:262
msgid "no stickers yet"
msgstr "cap adhesiu encara"

#: src/qml/Stickers/StickersPicker.qml:303
msgid "sent stickers will appear here"
msgstr "els adhesius enviats apareixeran aquí"

#: src/qml/ThreadDelegate.qml:37
msgid "New MMS notification"
msgstr "Nova notificació MMS"

#. TRANSLATORS: %1 is the first recipient the message is sent to, %2 is the count of remaining recipients
#: src/qml/ThreadDelegate.qml:78
#, qt-format
msgid "%1 + %2"
msgstr "%1 + %2"

#: src/qml/ThreadDelegate.qml:113
#, qt-format
msgid "Attachment: %1 image"
msgid_plural "Attachments: %1 images"
msgstr[0] "Adjunció: %1 imatge"
msgstr[1] "Adjunció: %1 imatges"

#: src/qml/ThreadDelegate.qml:116
#, qt-format
msgid "Attachment: %1 video"
msgid_plural "Attachments: %1 videos"
msgstr[0] "Adjunció: %1 vídeo"
msgstr[1] "Adjunció: %1 vídeos"

#: src/qml/ThreadDelegate.qml:119
#, qt-format
msgid "Attachment: %1 contact"
msgid_plural "Attachments: %1 contacts"
msgstr[0] "Adjunció: %1 contacte"
msgstr[1] "Adjunció: %1 contactes"

#: src/qml/ThreadDelegate.qml:122
#, qt-format
msgid "Attachment: %1 audio clip"
msgid_plural "Attachments: %1 audio clips"
msgstr[0] "Adjunció: %1 clip d'àudio"
msgstr[1] "Adjunció: %1 clips d'àudio"

#: src/qml/ThreadDelegate.qml:125
#, qt-format
msgid "Attachment: %1 file"
msgid_plural "Attachments: %1 files"
msgstr[0] "Adjunció: %1 fitxer"
msgstr[1] "Adjunció: %1 fitxers"

#: src/qml/ThreadDelegate.qml:363
msgid "Draft:"
msgstr "Esborrany:"

#, fuzzy
#~| msgid "Welcome to your Messaging app!"
#~ msgid "Welcome to your messaging app."
#~ msgstr "Us donem la benvinguda a l'aplicació de missatgeria!"

#~ msgid ""
#~ "Oops, there has been an error with the MMS system and this message could "
#~ "not be retrieved. Please ensure Cellular Data is ON and MMS settings are "
#~ "correct, then ask the sender to try again."
#~ msgstr ""
#~ "Sembla que hi ha hagut un error amb el sistema MMS i aquest missatge no "
#~ "s'ha pogut recuperar. Assegureu-vos de que la connexió de dades mòbils "
#~ "està connectada i la configuració MMS és correcta. Llavors demaneu que "
#~ "vos el tornin a enviar."

#~ msgid ""
#~ "Oops, there has been an error with the MMS system and this message could "
#~ "not be retrieved. Please ensure Cellular Data is ON and MMS settings are "
#~ "correct, then tap the redownload button to try to retrieve the message "
#~ "again."
#~ msgstr ""
#~ "Sembla que hi ha hagut un error amb el sistema MMS i aquest missatge no "
#~ "s'ha pogut recuperar. Assegureu-vos que la connexió de dades mòbils està "
#~ "disponible i la configuració MMS és correcta. Llavors demaneu que us el "
#~ "tornin a enviar."

#~ msgid "Add an online account"
#~ msgstr "Afegeix un compte en línia"

#~ msgid "Night Mode"
#~ msgstr "Mode nocturn"

#~ msgid "No"
#~ msgstr "No"

#~ msgid "Yes"
#~ msgstr "Sí"

#~ msgid "%1 hour call"
#~ msgid_plural "%1 hours call"
#~ msgstr[0] "Trucada d'%1 hora"
#~ msgstr[1] "Trucada de %1 hores"

#~ msgid "%1 minute call"
#~ msgid_plural "%1 minutes call"
#~ msgstr[0] "Trucada d'%1 minut"
#~ msgstr[1] "Trucada de %1 minuts"

#~ msgid "%1 second call"
#~ msgid_plural "%1 seconds call"
#~ msgstr[0] "Trucada d'%1 segon"
#~ msgstr[1] "Trucada de %1 segons"

#~ msgid "Missing message data"
#~ msgstr "Manquen dades de missatge"

#~ msgid "New Message"
#~ msgstr "Missatge nou"

#~ msgid "Create new"
#~ msgstr "Missatge nou"

#~ msgid "Later"
#~ msgstr "Més endavant"

#~ msgid "Switch to default SIM:"
#~ msgstr "Canvia a la SIM predeterminada:"

#~ msgid ""
#~ "Select a default SIM for all outgoing messages. You can always alter your "
#~ "choice in <a href=\"system_settings\">System Settings</a>."
#~ msgstr ""
#~ "Seleccioneu una targeta SIM per a tots els missatges de sortida. Podeu "
#~ "canviar aquesta opció sempre que vulgueu des dels  <a "
#~ "href=\"system_settings\">Paràmetres del sistema</a>."

#~ msgid "+ Create New"
#~ msgstr "+ Contacte nou"
