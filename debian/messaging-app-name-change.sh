#!/bin/sh

for base in "${XDG_CONFIG_HOME:-${HOME}/.config}" \
    "${XDG_DATA_HOME:-${HOME}/.local/share}" \
    "${XDG_CACHE_HOME:-${HOME}/.cache}"; do
    mv -T "${base}/com.ubuntu.messaging-app" \
        "${base}/messaging-app.ubports" 2>/dev/null
done

exit 0
